

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sale_product
-- ----------------------------
DROP TABLE IF EXISTS `sale_product`;
CREATE TABLE `sale_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_name` varchar(32) DEFAULT NULL COMMENT '商品名称',
  `product_info` varchar(128) DEFAULT NULL COMMENT '商品描述信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sale_product
-- ----------------------------
INSERT INTO `sale_product` VALUES ('1', '苹果手机', 'iphone20');
INSERT INTO `sale_product` VALUES ('2', '华为手机', 'huaweiP200');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_name` varchar(32) DEFAULT NULL COMMENT '用户名',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '张三', '我叫张三，来自第一个数据源');
INSERT INTO `sys_user` VALUES ('2', '张三的小弟', '我叫张三的小弟，来自第一个数据源');
