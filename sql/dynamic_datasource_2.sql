
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for log_store
-- ----------------------------
DROP TABLE IF EXISTS `log_store`;
CREATE TABLE `log_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_name` varchar(32) DEFAULT NULL COMMENT '仓库名称',
  `store_info` varchar(128) DEFAULT NULL COMMENT '仓库说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of log_store
-- ----------------------------
INSERT INTO `log_store` VALUES ('1', '华中仓库', '位于湖北武汉');
INSERT INTO `log_store` VALUES ('2', '华南仓库', '位于广东广州');
INSERT INTO `log_store` VALUES ('3', '华东仓库', '位于浙江杭州');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_name` varchar(32) DEFAULT NULL COMMENT '用户名',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '李四', '我叫李四，来之第二个数据源');
INSERT INTO `sys_user` VALUES ('2', '李四的大哥', '我叫李四的大哥，来之第二个数据源');
