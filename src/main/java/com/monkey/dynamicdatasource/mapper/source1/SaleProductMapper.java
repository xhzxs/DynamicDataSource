package com.monkey.dynamicdatasource.mapper.source1;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.monkey.dynamicdatasource.entity.source1.SaleProductEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@DS("master")
public interface SaleProductMapper extends BaseMapper<SaleProductEntity>{

}
