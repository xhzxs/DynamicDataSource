package com.monkey.dynamicdatasource.mapper.source2;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.monkey.dynamicdatasource.entity.source2.LogStoreEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@DS("source2")
public interface LogStoreMapper extends BaseMapper<LogStoreEntity>{

}
