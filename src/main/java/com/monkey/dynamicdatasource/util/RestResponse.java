package com.monkey.dynamicdatasource.util;

import java.util.HashMap;

public class RestResponse extends HashMap<String, Object> {
    public static RestResponse success(){
        return success("成功");
    }
    public static RestResponse success(String message){
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(true);
        restResponse.setMessage(message);
        restResponse.setCode("10000");
        return restResponse;
    }

    public static RestResponse failure(String message){
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(false);
        restResponse.setMessage(message);
        restResponse.setCode("20000");
        return restResponse;
    }

    public static RestResponse failure(String message,String code){
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(false);
        restResponse.setMessage(message);
        restResponse.setCode(code);
        return restResponse;
    }


    public RestResponse setSuccess(Boolean success) {
        if (success != null) {
            put("success", success);
        }
        return this;
    }

    public RestResponse setCode(String code) {
        if (code != null) {
            put("code", code);
        }
        return this;
    }

    public String getCode() {
        return (String)get("code");
    }

    public Boolean getSuccess() {
        return (Boolean)get("success");
    }

    public RestResponse setMessage(String message) {
        if (message != null){
            put("message", message);
        }
        return this;
    }

    public String getMessage() {
        return (String)get("message");
    }

    public RestResponse setData(Object data) {
        if (data != null) {
            put("data", data);
        }
        return this;
    }


    public RestResponse setAny(String key, Object value) {
        if (key != null && value != null) {
            put(key, value);
        }
        return this;
    }

}
