package com.monkey.dynamicdatasource.util;

import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import com.monkey.dynamicdatasource.config.DynamicDataSourceConfig;
import com.monkey.dynamicdatasource.exception.MyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DataSourceUtils {


    @Autowired
    private DynamicDataSourceConfig dynamicDataSourceConfig;


    /**
     * 调用mybatic plus 工具类
     * 手动为当前线程设置数据源
     *
     * @param dateSourcName 数据源名称
     */
    public void pushDataSource(String dateSourcName) {
        DynamicDataSourceContextHolder.push(dateSourcName);
        String dateSourceName = DynamicDataSourceContextHolder.peek();
        log.info("数据源设置提交：" + dateSourceName);
        //验证数据源是否切换成功
        if(dynamicDataSourceConfig.getDataSource().containsKey(dateSourceName) ){
            log.info("数据源设置成功：" + dateSourceName);
        } else {
            throw new MyException("数据源设置出错，数据源连接丢失或数据源不存在");
        }

    }



    /**
     * 调用mybatic plus 工具类
     * 手动清楚当前线程设置数据源
     */
    public void clearDataSource() {
        String dateSourceName = DynamicDataSourceContextHolder.peek();
        DynamicDataSourceContextHolder.clear();
        log.info("数据源清除成功：" + dateSourceName);
    }



}
