package com.monkey.dynamicdatasource.controller;


import com.monkey.dynamicdatasource.service.SysUserService;
import com.monkey.dynamicdatasource.service.source1.SaleProductService;
import com.monkey.dynamicdatasource.service.source2.LogStoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SaleProductService saleProductService;

    @Autowired
    private LogStoreService logStoreService;

    @GetMapping("/getSysUser")
    @ResponseBody
    public Object getSysUser(@RequestParam("dataSourceName") String dataSourceName) {
        return sysUserService.list(dataSourceName);
    }

    @GetMapping("/getSaleProduct")
    @ResponseBody
    public Object  getSaleProduct() {
        return saleProductService.list();
    }

    @GetMapping("/getLogStore")
    @ResponseBody
    public Object  getLogStore() {
        return logStoreService.list();
    }



}
