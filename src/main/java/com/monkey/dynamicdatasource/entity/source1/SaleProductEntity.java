package com.monkey.dynamicdatasource.entity.source1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@TableName("sale_product")
@Data
public class SaleProductEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @TableField("product_name")
    private String productName;

    @TableField("product_info")
    private String productInfo;



}
