package com.monkey.dynamicdatasource.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("sys_user")
@Data
public class SysUserEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @TableField("user_name")
    private String userName;

    @TableField("remark")
    private String remark;

}
