package com.monkey.dynamicdatasource.entity.source2;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@TableName("log_store")
@Data
public class LogStoreEntity {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @TableField("store_name")
    private String storeName;

    @TableField("store_info")
    private String storeInfo;
}
