package com.monkey.dynamicdatasource.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.monkey.dynamicdatasource.entity.SysUserEntity;

import java.util.List;

public interface SysUserService extends IService<SysUserEntity>{

    List<SysUserEntity> list(String dataSourceName);

}
