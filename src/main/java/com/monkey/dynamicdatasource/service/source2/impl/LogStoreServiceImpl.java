package com.monkey.dynamicdatasource.service.source2.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.monkey.dynamicdatasource.mapper.source2.LogStoreMapper;
import com.monkey.dynamicdatasource.entity.source2.LogStoreEntity;
import com.monkey.dynamicdatasource.service.source2.LogStoreService;
import org.springframework.stereotype.Service;


@Service
public class LogStoreServiceImpl extends ServiceImpl<LogStoreMapper, LogStoreEntity> implements LogStoreService {

}
