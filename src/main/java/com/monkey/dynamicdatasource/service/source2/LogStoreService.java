package com.monkey.dynamicdatasource.service.source2;

import com.baomidou.mybatisplus.extension.service.IService;
import com.monkey.dynamicdatasource.entity.source2.LogStoreEntity;

public interface LogStoreService extends IService<LogStoreEntity>{

}
