package com.monkey.dynamicdatasource.service.source1;

import com.baomidou.mybatisplus.extension.service.IService;
import com.monkey.dynamicdatasource.entity.source1.SaleProductEntity;

public interface SaleProductService extends IService<SaleProductEntity>{

}
