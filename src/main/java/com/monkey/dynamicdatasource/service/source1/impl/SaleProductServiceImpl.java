package com.monkey.dynamicdatasource.service.source1.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.monkey.dynamicdatasource.mapper.source1.SaleProductMapper;
import com.monkey.dynamicdatasource.entity.source1.SaleProductEntity;
import com.monkey.dynamicdatasource.service.source1.SaleProductService;
import org.springframework.stereotype.Service;


@Service
public class SaleProductServiceImpl extends ServiceImpl<SaleProductMapper, SaleProductEntity> implements SaleProductService {

}
