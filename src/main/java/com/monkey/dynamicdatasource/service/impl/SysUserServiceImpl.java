package com.monkey.dynamicdatasource.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.monkey.dynamicdatasource.mapper.SysUserMapper;
import com.monkey.dynamicdatasource.entity.SysUserEntity;
import com.monkey.dynamicdatasource.service.SysUserService;
import com.monkey.dynamicdatasource.util.DataSourceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {

    @Autowired
    private DataSourceUtils dataSourceUtils;

    @Override
    public List<SysUserEntity> list(String dataSourceName){
        dataSourceUtils.pushDataSource(dataSourceName);
        List<SysUserEntity> list=baseMapper.selectList(Wrappers.emptyWrapper());
        dataSourceUtils.clearDataSource();
        return list;
    }

}
